<?php

use App\Http\Controllers\CrudController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'PagesController@home');
Route::get('/about', 'PagesController@test');

Route::get('/karyawan', 'CrudController@index'); //Read
Route::get('/karyawan/create/', 'CrudController@create'); //Create
Route::get('/karyawan/detail/{employee}', 'CrudController@show'); //Show
Route::get('/karyawan/{employee}/edit', 'CrudController@edit'); //Edit

Route::post('/karyawan', 'CrudController@store'); //Save Create

Route::delete('/karyawan/{employee}', 'CrudController@destroy'); //Delete

Route::patch('/karyawan/{employee}', 'CrudController@update'); //Save Edit

//Jika menggunakan resource umum untuk menghandle CRUD maka bisa otomatis dengan 1 route dibawah
//Tidak bisa digunakan di Laravel 8
// Route::resource('karyawan', 'CrudController');