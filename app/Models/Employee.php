<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['nama', 'nik', 'alamat', 'tanggal_lahir', 'pendidikan_terakhir', 'no_hp'];

    // public function education()
    // {
    //     return $this->hasOne(Education::class);
    // }
}