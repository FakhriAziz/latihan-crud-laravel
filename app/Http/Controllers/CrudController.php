<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use App\Models\Education;


class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $employee = Employee::all(); //Eloquent ORM
        // $join = Education::find(5)->employee;
        
        $karyawan = DB::table('tabel_karyawan')->get(); //Query Builder
        
        // $employee = DB::table('employees') //sementara pakai query builder untuk join
        //     ->join('select_pendidikan', 'employees.pendidikan_terakhir', '=', 'select_pendidikan.id')
        //     ->select('employees.*', 'select_pendidikan.nama_pendidikan')
        //     ->get();
        
        $cobaJoin = Education::join('employees', 'educations.id', '=', 'employees.education_id')
        ->get(['employees.*', 'educations.nama as nama_edu']);
        // dd($cobaJoin);
        
        return view('karyawan/index', ['karyawan' => $karyawan], ['employee' => $cobaJoin]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $select = Education::all();
        return view('karyawan/create', ['select' => $select]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $employee = new Employee;
        // $employee->nama = $request->nama;
        // $employee->nik = $request->nik;
        // $employee->alamat = $request->alamat;
        // $employee->no_hp = $request->no_hp;
        // $employee->tanggal_lahir = $request->tanggal_lahir;
        // $employee->pendidikan_terakhir = $request->pendidikan_terakhir;
        // $employee->save();

        $request->validate([
            'nama' => 'required',
            'nik' => 'required|unique:employees,nik|size:6',
            'alamat' => 'required',
            'no_hp' => 'required',
            'tanggal_lahir' => 'required',
            'pendidikan_terakhir' => 'required'
            
        ]);

        Employee::create($request->all()); //with Eloquent ORM

        return redirect('/karyawan')->with('status', 'Berhasil buat data baru!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Education::join('employees', 'educations.id', '=', 'employees.education_id')
        ->where('employees.id', $id)
        ->first(['employees.*', 'educations.nama as nama_edu']);
        // ->first();
        // dd($employee);

        return view('karyawan/detail', ['employee' => $employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $select = Education::all();
        return view('karyawan/edit', ['employee' => $employee], ['select' => $select]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $request->validate([
            'nama' => 'required',
            'nik' => 'required|unique:employees,nik|size:6',
            'alamat' => 'required',
            'no_hp' => 'required',
            'tanggal_lahir' => 'required',
            'pendidikan_terakhir' => 'required'
            
        ]);
        
        Employee::where('id', $employee->id)
        ->update([
            'nama' => $request->nama,
            'nik' => $request->nik,
            'alamat' => $request->alamat,
            'no_hp' => $request->no_hp,
            'tanggal_lahir' => $request->tanggal_lahir,
            'pendidikan_terakhir' => $request->pendidikan_terakhir
        ]);
        return redirect('/karyawan')->with('status', 'Berhasil ubah data!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        Employee::destroy($employee->id);
        return redirect('/karyawan')->with('status', 'Berhasil hapus data!!');
    }
}
