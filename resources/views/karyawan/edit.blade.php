@extends('layout/main')

@section('title', 'Coba Laravel')

@section('container')
<div class="container">
    <div class="row my-2">
        <h1> Form Tambah Data </h1>
    </div>
    <div class="row mb-5">
        <div class="col-10">
            <!-- Menggunakan method form post tapi method patch untuk fungsi update dari form -->
            <form method="post" action="/karyawan/{{ $employee->id }}" class="row g-3">
                @method('patch')
                @csrf
                <div class="col-md-6 mb-3">
                    <label for="nama" class="form-label">Nama : </label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ $employee->nama }}" aria-describedby="emailHelp">
                    @error('nama')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-6 mb-3">
                    <label for="nik" class="form-label">NIK : </label>
                    <input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" name="nik" value="{{ $employee->nik }}" aria-describedby="emailHelp">
                    @error('nik')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-6 mb-3">
                    <label for="alamat" class="form-label">Alamat : </label>
                    <input type="text" class="form-control @error('alamat') is-invalid @enderror" id="alamat" name="alamat" value="{{ $employee->alamat }}" aria-describedby="emailHelp">
                    @error('alamat')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-6 mb-3">
                    <label for="no_hp" class="form-label">No. HP : </label>
                    <input type="text" class="form-control @error('no_hp') is-invalid @enderror" id="no_hp" name="no_hp" value="{{ $employee->no_hp }}" aria-describedby="emailHelp">
                    @error('no_hp')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-6 mb-3">
                    <label for="tanggal_lahir" class="form-label">Tanggal Lahir : </label>
                    <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" id="tanggal_lahir" name="tanggal_lahir" value="{{ $employee->tanggal_lahir }}" aria-describedby="emailHelp">
                    @error('tanggal_lahir')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-6 mb-3">
                    <label for="pendidikan_terakhir" class="form-label @error('pendidikan_terakhir') is-invalid @enderror">Pendidikan Terakhir : </label>
                    <select class="form-select" id="pendidikan_terakhir" name="pendidikan_terakhir" aria-label="Default select example">
                        @foreach($select as $sel)
                        <option value="{{ $sel->nama }}">{{ $sel->nama }}</option>
                        @endforeach
                    </select>
                    @error('pendidikan_terakhir')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection