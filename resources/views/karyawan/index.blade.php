@extends('layout/main')

@section('title', 'Coba Laravel')

@section('container')
<div class="container">
  <div class="row my-2">
    <h1> Tabel with Query Builder </h1>
  </div>
  <div class="row mb-5">
    <div class="col-10">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">NIK</th>
            <th scope="col">Nama</th>
            <th scope="col">Alamat</th>
            <th scope="col">Tempat Lahir</th>
            <th scope="col">Pendidikan Terakhir</th>
            <th scope="col">No. HP</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($karyawan as $k)
          <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td>{{ $k->nik }}</td>
            <td>{{ $k->nama }}</td>
            <td>{{ $k->alamat }}</td>
            <td>{{ $k->tempat_lahir }}</td>
            <td>{{ $k->pendidikan_terakhir }}</td>
            <td>{{ $k->no_hp }}</td>
            <td>
              <a href="" class="badge bg-success">edit</a>
              <a href="" class="badge bg-danger">delete</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="container">
  <div class="row my-2">
    <h1> Tabel with Eloquent </h1>
  </div>
  <div class="row mb-5">
    <div class="col-10">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">NIK</th>
            <th scope="col">Nama</th>
            <th scope="col">Alamat</th>
            <th scope="col">Tanggal Lahir</th>
            <th scope="col">Pendidikan Terakhir</th>
            <th scope="col">No. HP</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($employee as $e)
          <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td>{{ $e->nik }}</td>
            <td>{{ $e->nama }}</td>
            <td>{{ $e->alamat }}</td>
            <td>{{ $e->tanggal_lahir }}</td>
            <td>{{ $e->nama_edu }}</td>
            <td>{{ $e->no_hp }}</td>
            <td>
              <a href="" class="badge bg-success">edit</a>
              <a href="" class="badge bg-danger">delete</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="container">
  <div class="row my-2">
    <h1> List with CRUD (Create, Read, Update & Delete) </h1>
  </div>
  <a href="/karyawan/create" class="btn btn-primary mb-2">Buat Data Baru</a>

  @if(session('status'))
  <div class="col-6">
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  </div>
  @endif
  <div class="row mb-5">
    <div class="col-6">
      <ol class="list-group list-group-numbered">
        @foreach($employee as $em)
        <li class="list-group-item d-flex justify-content-between align-items-start">
          <div class="ms-2 me-auto">
            {{ $em->nama }}
          </div>
          <a href="/karyawan/detail/{{ $em->id }}" class="badge bg-info">show</a>
          <!-- Jika menggunakan route resource -->
          <!-- <a href="/karyawan/{{ $em->id }}" class="badge bg-info">show</a> -->
        </li>
        @endforeach
      </ol>
    </div>
  </div>
</div>
@endsection