@extends('layout/main')

@section('title', 'Coba Laravel')

@section('container')
<div class="container">
    <div class="row my-2">
        <h1> Detail with Card </h1>
    </div>
    <div class="row mb-5">
        <div class="col-10">
            <div class="card">
                <div class="card-header">
                    Nama anda : {{ $employee->nama }}
                </div>
                <div class="card-body">
                    <p class="card-text">Alamat : {{ $employee->alamat }}</p>
                    <p class="card-text">Tanggal Lahir : {{ $employee->tanggal_lahir }}</p>
                    <p class="card-text">Pendidikan : {{ $employee->nama_edu }}</p>
                    <a href="/karyawan/{{ $employee->id }}/edit" class="btn btn-primary">Edit</a>
                    <!-- Menggunakan method form post tapi method route delete agar tidak delete manual url -->
                    <form action="/karyawan/{{ $employee->id }}" method="post" class="d-inline"> 
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    <a href="/karyawan" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection